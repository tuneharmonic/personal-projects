﻿using ADOExample.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOExample
{
    class Program
    {
        static void Main(string[] args)
        {
            bool loop = true;
            UIController ui = new UIController();

            while (loop)
            {
                ui.PrintMainMenu();
                int choice = ui.GetNumberedChoice("Please choose an option: ");
                switch (choice)
                {
                    case 1:
                        // Write new
                        ui.NewBlog();
                        break;
                    case 2:
                        // Read all
                        ui.AllBlogs();
                        break;
                    case 3:
                        // Read specific
                        ui.MyBlog();
                        break;
                    case 4:
                        // Quit
                        loop = false;
                        break;
                    default:
                        Console.WriteLine("Not a valid choice, reseting menu...");
                        Console.ReadLine();
                        break;
                }
            }

            Console.WriteLine();
            Console.WriteLine("Goodbye!");
            Console.ReadLine();
        }
    }
}

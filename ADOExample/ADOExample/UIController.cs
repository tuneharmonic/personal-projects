﻿using ADOExample.Data;
using ADOExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOExample
{
    public class UIController
    {
        const string SEP = "--------------------------------------------------";
        DataStore data = new DataStore(@"Server=.\SQLEXPRESS;Initial Catalog=AdoExample;Integrated Security=true;");

        public void PrintMainMenu()
        {
            Console.Clear();

            Console.WriteLine("Main Menu");
            Console.WriteLine(SEP);
            Console.WriteLine();
            Console.WriteLine("1) Write a new blog");
            Console.WriteLine("2) See all blogs");
            Console.WriteLine("3) See a specific blog");
            Console.WriteLine();
            Console.WriteLine("4) Quit");
            Console.WriteLine();
            Console.WriteLine(SEP);
            Console.WriteLine();
            // Console.Write("Please Enter Choice: ");
        }

        public void AllBlogs()
        {
            Console.Clear();

            List<Blog> blogs = data.SelectAll();

            foreach (var blog in blogs)
            {
                Console.WriteLine(blog.Title);
                Console.WriteLine($"By {blog.Author}");
                Console.WriteLine(blog.Content);
                Console.WriteLine();
            }

            Console.ReadLine();
        }

        public void MyBlog()
        {
            Console.Clear();
            int id = GetNumberedChoice("Read which blog (ID #)? ");

            Console.Clear();

            Blog blog = data.Select(id);
            if (blog != null)
            {
                Console.WriteLine(blog.Title);
                Console.WriteLine($"By {blog.Author}");
                Console.WriteLine(blog.Content);
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Blog not found!");
            }
            Console.ReadLine();
        }

        public void NewBlog()
        {
            Console.Clear();

            string title = GetString("Enter a title: ");
            Console.WriteLine();
            string author = GetString("Enter an author: ");
            Console.WriteLine();
            string content = GetString("Enter content: ");
            Console.WriteLine();

            Console.WriteLine("Saving...");
            Blog myBlog = new Blog()
            {
                Title = title,
                Author = author,
                Content = content
            };

            if (data.Insert(myBlog))
            {
                Console.WriteLine("Blog successfully saved!");
            }
            else
            {
                Console.WriteLine("An error occurred...");
            }
            Console.ReadLine();
        }

        public int GetNumberedChoice(string prompt)
        {
            int number;
            do
            {
                Console.Write(prompt);
            } while (!int.TryParse(Console.ReadLine(), out number));
            return number;
        }

        public string GetString(string prompt)
        {
            string result = null;

            while (string.IsNullOrEmpty(result))
            {
                Console.WriteLine(prompt);
                result = Console.ReadLine();
            }

            return result;
        }
    }
}

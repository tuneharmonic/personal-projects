﻿using ADOExample.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOExample.Data
{
    public class DataStore
    {
        private readonly string connString;

        public DataStore(string conn)
        {
            connString = conn;
        }

        /// <summary>
        /// Inserts a new blog entry into the database
        /// </summary>
        /// <param name="blog">The blog entry to insert</param>
        /// <returns>True if successful, false if not</returns>
        public bool Insert(Blog blog)
        {
            bool success = false;

            using (SqlConnection conn = new SqlConnection(connString))
            {
                SqlCommand command = new SqlCommand("INSERT INTO Blogs VALUES ( @Title, @Author, @Content )", conn);
                command.CommandType = System.Data.CommandType.Text;

                command.Parameters.AddWithValue("@Title", blog.Title);
                command.Parameters.AddWithValue("@Author", blog.Author);
                command.Parameters.AddWithValue("@Content", blog.Content);

                conn.Open();

                success = command.ExecuteNonQuery() > 0;
            }

            return success;
        }

        /// <summary>
        /// Selects all blogs in the db
        /// </summary>
        /// <returns>A list of all blogs</returns>
        public List<Blog> SelectAll()
        {
            List<Blog> results = new List<Blog>();

            using (SqlConnection conn = new SqlConnection(connString))
            {
                SqlCommand command = new SqlCommand("SELECT * FROM Blogs", conn);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    results.Add(new Blog()
                    {
                        BlogID = reader.GetInt32(0),
                        Title = reader.GetString(1),
                        Author = reader.GetString(2),
                        Content = reader.GetString(3)
                    });
                }
            }

            return results;
        }

        /// <summary>
        /// Selects one blog by id
        /// </summary>
        /// <param name="id">The ID of the blog to find</param>
        /// <returns>The specified blog</returns>
        public Blog Select(int id)
        {
            Blog result = null;

            using (SqlConnection conn = new SqlConnection(connString))
            {
                SqlCommand command = new SqlCommand("SELECT * FROM Blogs WHERE BlogID = @BlogId", conn);
                command.Parameters.AddWithValue("@BlogId", id);
                conn.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    result = new Blog()
                    {
                        BlogID = reader.GetInt32(0),
                        Title = reader.GetString(1),
                        Author = reader.GetString(2),
                        Content = reader.GetString(3)
                    };
                }
            }

            return result;
        }
    }
}

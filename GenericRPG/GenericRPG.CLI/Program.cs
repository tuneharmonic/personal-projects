﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            while (!game.IsGameOver())
            {
                Console.Clear();
                game.StartTurn();
                Console.ReadLine();
                game.NextTurn();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public abstract class Weapon
    {
        public int Damage { get; set; }
        public abstract string GetName();
    }
}

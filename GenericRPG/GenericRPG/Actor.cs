﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public abstract class Actor
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public int MaxHealth { get; protected set; }
        public Weapon Weapon { get; set; }
        public int Defense { get; set; }
        public decimal Accuracy { get; set; }
        public abstract int Attack(Actor target);
        public abstract void GetAction();
        public int RollDie(int sides)
        {
            if (sides > 0)
            {
                Random rng = new Random();
                return rng.Next(1, sides + 1);
            }
            return 1;
        }
    }
}

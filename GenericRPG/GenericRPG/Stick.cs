﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public class Stick : Weapon
    {
        public Stick(int damage)
        {
            Damage = damage;
        }

        public override string GetName()
        {
            return "stick";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public class Game
    {
        private Player player;
        private Enemy enemy;

        private Random rng = new Random();

        private bool playerTurn;
        private bool gameOver;

        public Game()
        {
            player = new Player("Fred");
            enemy = new Enemy();

            playerTurn = rng.NextDouble() >= 0.5;
            gameOver = false;
        }

        public void StartTurn()
        {
            if (player.Health <= 0)
            {
                Console.WriteLine($"{player.Name} has died! Better luck next time!");
                gameOver = true;
                return;
            }

            if (enemy.Health <= 0)
            {
                Console.WriteLine($"{enemy.Name} has perished. Glory to the victor!");
                gameOver = true;
                return;
            }

            if (playerTurn)
            {
                Console.WriteLine($"It's {player.Name}'s turn!");
                Console.ReadLine();
                GetPlayerAction();
                ////AttackOther(player, enemy);
            }
            else
            {
                Console.WriteLine($"It's {enemy.Name}'s turn!");
                AttackOther(enemy, player);
            }
        }

        public void NextTurn()
        {
            playerTurn = !playerTurn;
        }

        public void ShowPlayerInventory()
        {
            if (player.Inventory.Any())
            {
                bool valid = false;
                while (!valid)
                {
                    Console.Clear();
                    Console.WriteLine("Inventory:");
                    for (int i = 0; i < player.Inventory.Count; i++)
                    {
                        Console.WriteLine($"{i + 1}) {player.Inventory[i].Name}");
                    }
                    Console.WriteLine($"{player.Inventory.Count + 1}) Go Back");
                    Console.Write("Enter choice: ");
                    valid = int.TryParse(Console.ReadLine(), out int choice);
                    if (valid)
                    {
                        if (choice < 1 || choice > player.Inventory.Count + 1)
                        {
                            valid = false;
                            Console.WriteLine("Invalid choice, please choose again");
                            Console.ReadLine();
                        }
                        else
                        {
                            if (choice != player.Inventory.Count + 1)
                            {
                                string desc = player.Inventory[choice - 1].Use(player);
                                player.Inventory.RemoveAt(choice - 1);
                                Console.WriteLine(desc);
                            }
                            else
                            {
                                // Back to player menu
                                GetPlayerAction();
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid choice, please choose again");
                        Console.ReadLine();
                    }
                }
            }
            else
            {
                Console.WriteLine("Inventory is empty!");
                Console.ReadLine();
                GetPlayerAction();
            }
        }

        public void GetPlayerAction()
        {
            bool valid = false;
            while (!valid)
            {
                Console.Clear();
                Console.WriteLine($"What does {player.Name} do?");
                Console.WriteLine($"Health: {player.Health}");
                Console.WriteLine("1) Attack");
                Console.WriteLine("2) Use Item");
                Console.WriteLine("3) Run away (quit)");
                Console.Write("Enter your choice: ");
                valid = int.TryParse(Console.ReadLine(), out int choice);
                if (valid)
                {
                    // Perform choice
                    switch (choice)
                    {
                        case 1:
                            AttackOther(player, enemy);
                            break;
                        case 2:
                            ShowPlayerInventory();
                            break;
                        case 3:
                            Console.WriteLine("Evil triumphs!");
                            gameOver = true;
                            return;
                        default:
                            valid = false;
                            Console.WriteLine("Invalid choice, please choose again");
                            Console.ReadLine();
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid choice, please choose again");
                    Console.ReadLine();
                }
            }
        }

        public void AttackOther(Actor subject, Actor target)
        {
            var damage = subject.Attack(target);
            if (damage > 0)
            {
                Console.WriteLine($"{subject.Name} did {damage} damage to {target.Name} with their {subject.Weapon.GetName()}!");
            }
            else
            {
                Console.WriteLine($"{subject.Name} missed their attack!");
            }
        }

        public bool IsGameOver()
        {
            return gameOver;
        }

        public bool IsPlayerTurn()
        {
            return playerTurn;
        }

        public string GetPlayerName()
        {
            return player.Name;
        }

        public string GetEnemyName()
        {
            return enemy.Name;
        }
    }
}

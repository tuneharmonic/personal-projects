﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public class Potion : Item
    {
        public Potion()
        {
            Name = "Potion";
        }

        public override string Use(Actor target)
        {
            int healthGap = target.MaxHealth - target.Health;
            if (healthGap > 20)
            {
                healthGap = 20;
            }
            target.Health += healthGap;

            return $"{target.Name} gained {healthGap} hit points!";
        }
    }
}

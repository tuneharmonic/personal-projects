﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public abstract class Item
    {
        public string Name { get; set; }
        public abstract string Use(Actor target);
    }
}

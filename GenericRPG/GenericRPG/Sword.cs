﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public class Sword : Weapon
    {
        public Sword(int damage)
        {
            Damage = damage;
        }

        public override string GetName()
        {
            return "sword";
        }
    }
}

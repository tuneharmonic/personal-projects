﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public class Enemy : Actor
    {
        public Enemy()
        {
            Name = "Goblin";
            Health = 30;
            MaxHealth = Health;
            Weapon = new Stick(5);
            Defense = 10;
            Accuracy = 0.8m;
        }

        public override int Attack(Actor target)
        {
            var attackAccuracy = RollDie(20) * Accuracy;
            if (attackAccuracy >= target.Defense)
            {
                var attackDamage = RollDie(this.Weapon.Damage);
                target.Health -= attackDamage;
                return attackDamage;
            }
            return 0;
        }

        public override void GetAction()
        {
            throw new NotImplementedException();
        }
    }
}

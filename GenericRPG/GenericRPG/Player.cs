﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericRPG
{
    public class Player : Actor
    {
        public List<Item> Inventory { get; set; }

        public Player(string name)
        {
            Name = name;
            Health = 30;
            MaxHealth = Health;
            Weapon = new Sword(10);
            Defense = 13;
            Accuracy = 0.8m;
            Inventory = new List<Item>
            {
                new Potion(),
                new Potion(),
                new Potion()
            };
        }

        public override int Attack(Actor target)
        {
            var attackAccuracy = RollDie(20) * Accuracy;
            if (attackAccuracy >= target.Defense)
            {
                var attackDamage = RollDie(this.Weapon.Damage);
                target.Health -= attackDamage;
                return attackDamage;
            }
            return 0;
        }

        public override void GetAction()
        {
            throw new NotImplementedException();
        }
    }
}

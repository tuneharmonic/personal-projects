﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPIPractice.Models;

namespace WebAPIPractice.Controllers
{
    public class NameController : ApiController
    {
        private static List<string> Names = new List<string>()
        {
            "Bobby",
            "Sue",
            "Jarrod",
            "Yang",
            "Kyle"
        };

        // GET: api/Name
        public IEnumerable<string> Get()
        {
            return Names;
        }

        // GET: api/Name/5
        public string Get(int id)
        {
            if (id >= 0 && id < Names.Count)
            {
                return Names[id];
            }
            return "Name not found";
        }

        // POST: api/Name
        public string Post([FromBody]NameRequest request)
        {
            if (!string.IsNullOrEmpty(request.Value) && !string.IsNullOrWhiteSpace(request.Value))
            {
                if (!Names.Contains(request.Value))
                {
                    Names.Add(request.Value);
                    return "Success: " + request.Value + " added";
                }
                return "Error: " + request.Value + " already exists";
            }
            return "Error: name is required";
        }

        // PUT: api/Name/5
        public string Put(int id, [FromBody]NameRequest request)
        {
            if (id >= 0 && id < Names.Count)
            {
                if (!string.IsNullOrEmpty(request.Value) && !string.IsNullOrWhiteSpace(request.Value))
                {
                    string oldName = Names[id];
                    Names[id] = request.Value;
                    return "Success: " + oldName + " was changed to " + Names[id];
                }
                return "Error: new name is required";
            }
            return "Error: no name exists at index " + id;
        }

        // DELETE: api/Name/5
        public string Delete(int id)
        {
            if (id >= 0 && id < Names.Count)
            {
                string deletedName = Names[id];
                Names.RemoveAt(id);
                return "Success: " + deletedName + " was deleted";
            }
            return "Error: no name exists at index " + id;
        }
    }
}

﻿using PartyTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartyTracker.DAL
{
    public abstract class BaseDataStore
    {
        /// <summary>
        /// Saves a new character to memory
        /// </summary>
        /// <param name="character">The character to save</param>
        public abstract void SaveCharacter(Character character);

        /// <summary>
        /// Gets all characters in memory
        /// </summary>
        /// <returns>A list of all characters</returns>
        public abstract List<Character> GetParty();

        /// <summary>
        /// Gets a specific character by name
        /// </summary>
        /// <param name="name">The name of the character to retrieve</param>
        /// <returns></returns>
        public abstract Character GetCharacter(string name);

        /// <summary>
        /// Updates a character already in memory
        /// </summary>
        /// <param name="name">The name of the character to update</param>
        /// <param name="character">The updated character data</param>
        public abstract void UpdateCharacter(string name, Character character);

        /// <summary>
        /// Deletes a character from memory
        /// </summary>
        /// <param name="name">The name of the character to delete</param>
        public abstract void DeleteCharacter(string name);
    }
}

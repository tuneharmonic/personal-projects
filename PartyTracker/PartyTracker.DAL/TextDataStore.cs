﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PartyTracker.Models;
using System.IO;

namespace PartyTracker.DAL
{
    public class TextDataStore : BaseDataStore
    {
        private string filePath = ConfigurationManager.AppSettings["FilePath"];
        private string fileName = ConfigurationManager.AppSettings["FileName"];
        private char delim = ConfigurationManager.AppSettings["Delimiter"].ToCharArray().FirstOrDefault();

        public override void DeleteCharacter(string name)
        {
            throw new NotImplementedException();
        }

        public override Character GetCharacter(string name)
        {
            throw new NotImplementedException();
        }

        public override List<Character> GetParty()
        {
            List<Character> party = new List<Character>();

            if (File.Exists(filePath + fileName))
            {
                using (StreamReader sr = new StreamReader(filePath + fileName))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] traits = line.Split(delim);
                        Character character = new Character()
                        {
                            // Name, AmmoInClip, ClipSize, ThermalClips, BioticCooldown, TechCooldown, BioticPoints, BioticPointsTotal, BioticMaintenance, TechPoints, TechPointsTotal, TechMaintenance, CoverIndex
                            Name = traits[0],
                            AmmoInClip = int.Parse(traits[1]),
                            ClipSize = int.Parse(traits[2]),
                            ThermalClips = int.Parse(traits[3]),
                            BioticCooldown = int.Parse(traits[4]),
                            TechCooldown = int.Parse(traits[5]),
                            BioticPoints = int.Parse(traits[6]),
                            BioticPointsTotal = int.Parse(traits[7]),
                            BioticMaintenance = int.Parse(traits[8]),
                            TechPoints = int.Parse(traits[9]),
                            TechPointsTotal = int.Parse(traits[10]),
                            TechMaintenance = int.Parse(traits[11]),
                            CoverIndex = decimal.Parse(traits[12])
                        };
                        party.Add(character);
                    }
                }
            }

            return party;
        }

        public override void SaveCharacter(Character character)
        {
            // Name, AmmoInClip, ClipSize, ThermalClips, BioticCooldown, TechCooldown, BioticPoints, BioticPointsTotal, BioticMaintenance, TechPoints, TechPointsTotal, TechMaintenance, CoverIndex
            string saveFormat = "{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}{0}{13}";
            if (!Directory.Exists(filePath))    // StreamWriter raises an exception if directory is not found
            {
                Directory.CreateDirectory(filePath);    // So, create it if it is not found
            }
            using (StreamWriter sw = new StreamWriter(filePath + fileName, true))
            {
                sw.WriteLine(string.Format(saveFormat, delim, character.Name, character.AmmoInClip, character.ClipSize,
                    character.ThermalClips, character.BioticCooldown, character.TechCooldown, character.BioticPoints,
                    character.BioticPointsTotal, character.BioticMaintenance, character.TechPoints, character.TechPointsTotal,
                    character.TechMaintenance, character.CoverIndex));
            }
        }

        public override void UpdateCharacter(string name, Character character)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using PartyTracker.BLL;
using PartyTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartyTracker.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            Tracker tracker = new Tracker();

            Console.WriteLine("Welcome! I'm about to save Bill to file");
            Console.WriteLine("Creating Bill...");

            Character bill = new Character()
            {
                Name = "Bill",
                ClipSize = 30,
                AmmoInClip = 30,
                ThermalClips = 12,
                CoverIndex = 0.5m,

                BioticCooldown = 0,
                BioticPointsTotal = 0,
                BioticPoints = 0,
                BioticMaintenance = 0,

                TechCooldown = 0,
                TechPointsTotal = 10,
                TechPoints = 10,
                TechMaintenance = 0
            };

            Console.WriteLine("Now saving Bill...");

            tracker.SaveMyCharacter(bill);

            Console.WriteLine("Bill is saved!");
            Console.ReadLine();
            Console.WriteLine("Now let's load our party back in...");

            List<Character> party = tracker.GetMyParty();

            if (party.Any())
            {
                Console.WriteLine("Party loaded! Let's try to find Bill...");
                Character bill2 = party.FirstOrDefault(c => c.Name == "Bill");
                if (bill2 != null)
                {
                    Console.WriteLine("Bill was found! Check it out:");
                    Console.WriteLine($"Name: {bill2.Name}, Current Ammo: {bill2.AmmoInClip} rounds, Cover Defense Bonus: +{bill2.GetCoverDefenseBonus()}");
                }
                else
                {
                    Console.WriteLine("Hmm... couldn't find Bill...");
                }
            }
            else
            {
                Console.WriteLine("Oops... looks like we couldn't find the party...");
            }

            Console.WriteLine("Mkay, bye!");
            Console.ReadLine();
        }
    }
}

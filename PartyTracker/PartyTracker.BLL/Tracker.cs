﻿using PartyTracker.DAL;
using PartyTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartyTracker.BLL
{
    public class Tracker
    {
        BaseDataStore data = new TextDataStore();

        public void SaveMyCharacter(Character character)
        {
            data.SaveCharacter(character);
        }

        public List<Character> GetMyParty()
        {
            return data.GetParty();
        }
    }
}

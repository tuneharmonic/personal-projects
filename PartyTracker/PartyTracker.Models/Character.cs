﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartyTracker.Models
{
    public class Character
    {
        /// <summary>
        /// The character's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ammo currently in weapon's clip
        /// </summary>
        public int AmmoInClip { get; set; }

        /// <summary>
        /// Total size of current weapon's clip
        /// </summary>
        public int ClipSize { get; set; }

        /// <summary>
        /// Number of thermal clips in inventory
        /// </summary>
        public int ThermalClips { get; set; }

        /// <summary>
        /// Current number of actions in biotic power cooldown timer
        /// </summary>
        public int BioticCooldown { get; set; }

        /// <summary>
        /// Current number of actions in tech power cooldown timer
        /// </summary>
        public int TechCooldown { get; set; }

        /// <summary>
        /// Current number of biotic points available to the character
        /// </summary>
        public int BioticPoints { get; set; }

        /// <summary>
        /// Total number of biotic points the character posesses
        /// </summary>
        public int BioticPointsTotal { get; set; }

        /// <summary>
        /// Current number of points reserved for maintenance of biotic powers
        /// </summary>
        public int BioticMaintenance { get; set; }

        /// <summary>
        /// Current number of tech points available to the character
        /// </summary>
        public int TechPoints { get; set; }

        /// <summary>
        /// Total number of tech points the character posesses
        /// </summary>
        public int TechPointsTotal { get; set; }

        /// <summary>
        /// Current number of points reserved for maintenance of tech powers
        /// </summary>
        public int TechMaintenance { get; set; }

        /// <summary>
        /// The current cover index of the player, should be a decimal from 0.0 to 1.0
        /// </summary>
        public decimal CoverIndex { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class.
        /// </summary>
        public Character()
        {
            Name = "Bob";
            ClipSize = 0;
            AmmoInClip = 0;
            ThermalClips = 0;
            CoverIndex = 0m;

            BioticCooldown = 0;
            BioticPointsTotal = 0;
            BioticPoints = BioticPointsTotal;
            BioticMaintenance = 0;

            TechCooldown = 0;
            TechPointsTotal = 0;
            TechPoints = TechPointsTotal;
            TechMaintenance = 0;
        }

        /// <summary>
        /// Gets the bonus applied to character's defense based on cover index
        /// </summary>
        /// <returns>The bonus to this character's defense</returns>
        public int GetCoverDefenseBonus()
        {
            if (CoverIndex <= 0m && CoverIndex < 0.25m)
            {
                return 0;
            }
            else if (CoverIndex >= 0.25m && CoverIndex < 0.5m)
            {
                return 2;
            }
            else if (CoverIndex >= 0.5m && CoverIndex < 0.75m)
            {
                return 4;
            }
            else if (CoverIndex >= 0.75m && CoverIndex < 0.9m)
            {
                return 7;
            }
            else if (CoverIndex >= 0.9m && CoverIndex < 1.0m)
            {
                return 10;
            }
            else
            {
                return 100; // Basically invincible, full cover
            }
        }

        /// <summary>
        /// Moves the character bewteen different levels of cover
        /// </summary>
        /// <param name="coverIndex">The new cover index to set. Should be between 0.0 and 1.0</param>
        public void GetToCover(decimal coverIndex)
        {
            CoverIndex = coverIndex;
        }

        /// <summary>
        /// Switches to a new weapon, assumes clip is full
        /// </summary>
        /// <param name="newClipSize">The size of the new weapon's magazine</param>
        public void SwitchWeapon(int newClipSize)
        {
            ClipSize = newClipSize;
            AmmoInClip = newClipSize;
        }

        /// <summary>
        /// Switches to a new weapon
        /// </summary>
        /// <param name="newClipSize">The clip size of the new weapon</param>
        /// <param name="roundsInClip">The number of rounds currently in the new weapon's clip</param>
        public void SwitchWeapon(int newClipSize, int roundsInClip)
        {
            ClipSize = newClipSize;
            AmmoInClip = roundsInClip;
        }

        /// <summary>
        /// Reloads currently equipped weapon
        /// </summary>
        public void ReloadWeapon()
        {
            ThermalClips--;
            AmmoInClip = ClipSize;
        }

        /// <summary>
        /// Uses a biotic power if able
        /// </summary>
        /// <param name="cost">The usage cost of the power</param>
        /// <param name="maintenance">The maintenance cost of the power</param>
        /// <param name="cooldown">The number of actions this power needs to cool down</param>
        public void UseBioticPower(int cost, int maintenance, int cooldown)
        {
            if (cooldown == 0 && BioticPoints >= cost + maintenance)
            {
                BioticPoints -= cost + maintenance;
                BioticCooldown += cooldown;
            }
        }

        /// <summary>
        /// Uses a tech power if able
        /// </summary>
        /// <param name="cost">The usage cost of the power</param>
        /// <param name="maintenance">The maintenance cost of the power</param>
        /// <param name="cooldown">The number of actions this power needs to cool down</param>
        public void UseTechPower(int cost, int maintenance, int cooldown)
        {
            if (cooldown == 0 && TechPoints >= cost + maintenance)
            {
                TechPoints -= cost + maintenance;
                TechCooldown += cooldown;
            }
        }

        /// <summary>
        /// Attempts to restore a given number of biotic points
        /// </summary>
        /// <param name="points">The number of points to restore</param>
        public void RestoreBioticPoints(int points)
        {
            if (BioticPoints + points <= BioticPointsTotal - BioticMaintenance)
            {
                BioticPoints += points;
            }
            else
            {
                BioticPoints = BioticPointsTotal - BioticMaintenance;
            }
            // BioticPoints = (BioticPoints + points <= BioticPointsTotal) ? BioticPoints + points : BioticPointsTotal;
        }

        /// <summary>
        /// Attempts to restore a given number of tech points
        /// </summary>
        /// <param name="points">The number of points to restore</param>
        public void RestoreTechPoints(int points)
        {
            if (TechPoints + points <= TechPointsTotal - TechMaintenance)
            {
                TechPoints += points;
            }
            else
            {
                TechPoints = TechPointsTotal - TechMaintenance;
            }
            // TechPoints = (TechPoints + points <= TechPointsTotal) ? TechPoints + points : TechPointsTotal;
        }

        /// <summary>
        /// Dismisses an active biotic power
        /// </summary>
        /// <param name="maintenance">The maintenance cost of the active power</param>
        public void DismissBioticPower(int maintenance)
        {
            if (BioticMaintenance - maintenance >= 0)
            {
                BioticMaintenance -= maintenance;
            }
            else
            {
                BioticMaintenance = 0;
            }
        }

        /// <summary>
        /// Dismisses an active tech power
        /// </summary>
        /// <param name="maintenance">The maintenance cost of the active power</param>
        public void DismissTechPower(int maintenance)
        {
            if (TechMaintenance - maintenance >= 0)
            {
                TechMaintenance -= maintenance;
            }
            else
            {
                TechMaintenance = 0;
            }
        }
    }
}

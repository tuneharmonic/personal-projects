﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLIDemo.Data.Interfaces;
using SQLIDemo.Models;
using SQLIDemo.Data;

namespace SQLIDemo.BLL
{
    public class Manager
    {
        private IDataStore _dataStore = DataStoreFactory.Create();

        public List<Employee> GetEmployeeList(string search)
        {
            var getEmployees = _dataStore.GetEmployees(search);
            return getEmployees;
        }
    }
}
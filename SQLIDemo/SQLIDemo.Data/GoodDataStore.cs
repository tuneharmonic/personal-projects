﻿using SQLIDemo.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLIDemo.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace SQLIDemo.Data
{
    public class GoodDataStore : IDataStore
    {
        public List<Employee> GetEmployees(string search)
        {
            List<Employee> employees = new List<Employee>();

            // Configuration-defined connection string, uses integrated security with limited permissions   <---    SAFER & EASIER
            string cs = ConfigurationManager.ConnectionStrings["GoodConnString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                // Parameterized query virtually eliminates possibility of injection attack     <---    WAAAAYYYY SAFER
                SqlCommand cmd = new SqlCommand("SELECT * FROM Employee WHERE FirstName like @EmployeeName", con);
                cmd.Parameters.Add("@EmployeeName", System.Data.SqlDbType.NVarChar, 50).Value = "%" + search + "%";

                //cmd.Parameters.AddWithValue("@EmployeeName", "%" + search + "%");     <---    Easier, but less efficient

                con.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        employees.Add(
                            new Employee()
                            {
                                FirstName = dr["FirstName"].ToString(),
                                LastName = dr["LastName"].ToString(),
                                EmpID = (int)dr["EmpID"]
                            }
                        );
                    }
                }
            }

            return employees;
        }
    }
}

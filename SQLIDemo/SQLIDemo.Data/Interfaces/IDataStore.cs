﻿using SQLIDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLIDemo.Data.Interfaces
{
    public interface IDataStore
    {
        List<Employee> GetEmployees(string search);
    }
}

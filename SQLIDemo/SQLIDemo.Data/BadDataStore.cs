﻿using SQLIDemo.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLIDemo.Models;
using System.Data.SqlClient;

namespace SQLIDemo.Data
{
    public class BadDataStore : IDataStore
    {
        public List<Employee> GetEmployees(string search)
        {
            List<Employee> employees = new List<Employee>();

            // Hard-coded connection string, uses a system admin account with credentials    <---   VERY UNSAFE &
                                                                                                //  HARD TO DEPLOY
            string cs = @"Server=.\SQLEXPRESS;Database=Zoom;User Id=sa;Password=apprentice;";
            using (SqlConnection con = new SqlConnection(cs))
            {
                // Concatenated query opens the doors to nasty SQL Injection    <---    SOOOO UNSAFE
                SqlCommand cmd = new SqlCommand("SELECT * FROM Employee WHERE FirstName like '%" + search + "%'", con);
                con.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        employees.Add(
                            new Employee()
                            {
                                FirstName = dr["FirstName"].ToString(),
                                LastName = dr["LastName"].ToString(),
                                EmpID = (int) dr["EmpID"]
                            }
                        );
                    }
                }
            }

            return employees;
        }
    }
}

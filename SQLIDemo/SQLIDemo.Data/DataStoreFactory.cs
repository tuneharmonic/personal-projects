﻿using SQLIDemo.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLIDemo.Data
{
    public static class DataStoreFactory
    {
        public static IDataStore Create()
        {
            string mode = ConfigurationManager.AppSettings["Quality"];
            switch (mode)
            {
                case "Bad":
                    return new BadDataStore();
                case "Good":
                    return new GoodDataStore();
                default:
                    return null;
            }
        }
    }
}

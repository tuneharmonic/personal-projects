﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLIDemo.Models;
using SQLIDemo.BLL;

namespace SQLIDemo.UI
{
    public class UiController
    {
        public void WorkFlow()
        {
            do
            {
                Display();
                SearchEmployees();
                Console.Write("Search again? (Y/N): ");
            } while (PromptYesNo());
        }

        public bool PromptYesNo()
        {
            string input = "";
            do
            {
                input = Console.ReadLine().Trim().ToLower();
            } while (input != "y" && input != "n");

            if (input == "y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Display()
        {
            Console.Clear();
            Console.WriteLine("====================================================");
            Console.WriteLine("|                                                  |");
            Console.WriteLine("|                 Search Employee                  |");
            Console.WriteLine("|                                                  |");
            Console.WriteLine("====================================================");
            Console.WriteLine();
        }

        public string GetUserName()
        {
            Console.Write("Provide a first name: ");
            var userInput = Console.ReadLine();
            return userInput;
        }

        public void SearchEmployees()
        {
            Manager man = new Manager();
            var employees = man.GetEmployeeList(GetUserName());
            foreach (var employee in employees)
            {
                Console.WriteLine($"First Name: {employee.FirstName}, Last Name: {employee.LastName} | Employee ID: {employee.EmpID}");
            }

            Console.ReadKey();
        }
    }
}

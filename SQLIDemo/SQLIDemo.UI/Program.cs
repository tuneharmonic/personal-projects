﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLIDemo.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "SQL Injections: Not on my Watch";
            UiController cont = new UiController();
            cont.WorkFlow();
        }
    }
}

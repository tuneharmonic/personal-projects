///shoot_blast(px, py, pdir)
bx = argument[0];
by = argument[1];
bdir = argument[2];

bobj = instance_create(bx, by, blastobj);
with bobj 
{
    bvx = (dcos(player.bdir) * 20);
    bvy = (-(dsin(player.bdir) * 20));
    physics_apply_impulse(phy_position_x, phy_position_y, bvx, bvy);
}

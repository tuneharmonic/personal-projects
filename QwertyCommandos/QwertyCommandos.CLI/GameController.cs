﻿using QwertyCommandos.GLL;
using QwertyCommandos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QwertyCommandos.CLI
{
    public class GameController
    {
        private QwertyGame game = new QwertyGame();

        public void MainMenu()
        {
            bool continuePlaying = true;
            do
            {
                Console.Clear();
                Console.WriteLine("Welcome to QWERTY Commandos!");
                Console.ReadKey();
                Console.WriteLine();
                Console.WriteLine("Top Players:");
                foreach (Player player in game.GetTopPlayers(3))
                {
                    Console.WriteLine($"{player.Name}  ---  {player.Time.TotalSeconds} seconds");
                }
                Console.WriteLine();
                Console.Write("Want to get some training, commando? (Y/N): ");
                continuePlaying = PromptYesOrNo();
                if (continuePlaying)
                {
                    ExplainRules();
                    StartGame();
                    Console.WriteLine("Return to Main Menu? (Y/N): ");
                    continuePlaying = PromptYesOrNo();
                    if (!continuePlaying)
                    {
                        Console.WriteLine("Until next time, commando! Press one last key...");
                        Console.ReadKey();
                    }
                }
                else
                {
                    Console.WriteLine("Suit yourself!");
                    Console.ReadKey();
                }
            } while (continuePlaying);
            
        }

        public void StartGame()
        {
            Console.Write("Enter your name, private: ");
            game.SetPlayerName(Console.ReadLine());
            Countdown(3);
            game.StartTimer();
            GetKeystrokes(100);
            game.StopTimer();
            game.SaveGame();
            Console.Clear();
            Console.WriteLine($"That's it! Your time was {game.GetPlayerTime().TotalSeconds} seconds...");
            Console.ReadKey();
            //Show if in high scores
        }

        public void ExplainRules()
        {
            Console.WriteLine("Alright, soldier, here's how this works: Give me your name, and on the count of three, start mashing keys! You need to give me at least 100 characters, and you WILL be timed.");
            Console.WriteLine("Any questions? No? Good!");
            Console.WriteLine("Press any key to get started...");
            Console.ReadKey();
        }

        public void GetKeystrokes(int count)
        {
            int counter = 0;
            while (counter < count)
            {
                Console.ReadKey();
                counter++;
            }
        }

        public void Countdown(int count)
        {
            for (int i = count; i > 0; i--)
            {
                Console.Clear();
                Console.Write(i);
                Thread.Sleep(1000);
            }
            Console.Clear();
            Console.WriteLine("GO!");
        }

        public bool PromptYesOrNo()
        {
            string prompt;

            do
            {
                prompt = Console.ReadLine().Trim().ToLower();
                if (!prompt.StartsWith("y") && !prompt.StartsWith("n"))
                {
                    Console.WriteLine("I can't read you, cadet...");
                }
            } while (!prompt.StartsWith("y") && !prompt.StartsWith("n"));

            if (prompt.StartsWith("y"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

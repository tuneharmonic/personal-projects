﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwertyCommandos.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            GameController controller = new GameController();
            controller.MainMenu();
        }
    }
}

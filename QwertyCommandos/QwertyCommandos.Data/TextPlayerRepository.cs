﻿using QwertyCommandos.Data.Interfaces;
using QwertyCommandos.Models;
using QwertyCommandos.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwertyCommandos.Data
{
    public class TextPlayerRepository : IPlayerRepository
    {
        private const string _dirPath = @"C:\MyData\Qwerty\";
        private const string _filepath = _dirPath + "hiscores.txt";
        private const char DELIM = ',';

        public TextPlayerRepository()
        {
            if (!Directory.Exists(_dirPath))
            {
                Directory.CreateDirectory(_dirPath);
                var file = File.Create(_filepath);
                using (StreamWriter sw = new StreamWriter(file))
                {
                    sw.WriteLine($"playerName{DELIM}playerTime");
                }
            }
            else
            {
                if (!File.Exists(_filepath))
                {
                    var file = File.Create(_filepath);
                    using (StreamWriter sw = new StreamWriter(file))
                    {
                        sw.WriteLine($"playerName{DELIM}playerTime");
                    }
                }
            }
        }

        public List<Player> GetAllPlayers()
        {
            List<Player> players = new List<Player>();

            using (StreamReader sr = new StreamReader(_filepath))
            {
                sr.ReadLine();

                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] splitLine = line.Split(DELIM);

                    Player player = new Player();

                    player.SetName(splitLine[0]);
                    player.SetTime(TimeSpan.Parse(splitLine[1]));

                    players.Add(player);
                }
            }

            return players;
        }

        public List<Player> GetTopPlayers(int numberOfPlayers)
        {
            List<Player> allPlayers = GetAllPlayers();

            int playersToGet = Math.Min(numberOfPlayers, allPlayers.Count);

            IEnumerable<Player> topPlayers = allPlayers.OrderBy(p => p.Time);

            return topPlayers.Take(playersToGet).ToList();
        }

        public void SavePlayers(List<Player> players)
        {
            using (StreamWriter sw = new StreamWriter(_filepath, false))
            {
                sw.WriteLine($"playerName{DELIM}playerTime");
                foreach (var player in players)
                {
                    sw.WriteLine($"{player.Name}{DELIM}{player.Time}");
                }
            }
        }

        public void SavePlayer(Player player)
        {
            List<Player> allPlayers = GetAllPlayers();

            allPlayers.Add(player);

            SavePlayers(allPlayers);
        }
    }
}

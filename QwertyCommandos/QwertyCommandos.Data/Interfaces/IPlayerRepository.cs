﻿using QwertyCommandos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwertyCommandos.Data.Interfaces
{
    public interface IPlayerRepository
    {
        List<Player> GetAllPlayers();
        List<Player> GetTopPlayers(int numberOfPlayers);
        void SavePlayers(List<Player> players);
        void SavePlayer(Player player);
    }
}

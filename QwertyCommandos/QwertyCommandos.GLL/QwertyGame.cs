﻿using QwertyCommandos.Data.Interfaces;
using QwertyCommandos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwertyCommandos.GLL
{
    public class QwertyGame
    {
        private IPlayerRepository _playerRepo = PlayerRepositoryFactory.Create();
        private Player _player = new Player();
        private DateTime _startTime;
        private DateTime _endTime;


        public void SaveGame()
        {
            _playerRepo.SavePlayer(_player);
        }

        public void SetPlayerName(string name)
        {
            _player.SetName(name);
        }

        public string GetPlayerName()
        {
            return _player.Name;
        }

        public void StartTimer()
        {
            _startTime = DateTime.Now;
        }

        public void StopTimer()
        {
            _endTime = DateTime.Now;

            _player.SetTime(_endTime - _startTime);
        }

        public TimeSpan GetPlayerTime()
        {
            return _player.Time;
        }

        public List<Player> GetTopPlayers(int count)
        {
            return _playerRepo.GetTopPlayers(count);
        }
    }
}

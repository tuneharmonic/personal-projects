﻿using QwertyCommandos.Data;
using QwertyCommandos.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwertyCommandos.GLL
{
    public static class PlayerRepositoryFactory
    {
        public static IPlayerRepository Create()
        {
            string mode = ConfigurationManager.AppSettings["SaveMode"].ToString();

            switch (mode)
            {
                case "Text":
                    return new TextPlayerRepository();
                default:
                    return null;
            }
        }
    }
}

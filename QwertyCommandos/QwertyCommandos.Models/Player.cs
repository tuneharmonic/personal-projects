﻿using QwertyCommandos.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwertyCommandos.Models
{
    public class Player
    {
        public string Name { get; private set; }
        public TimeSpan Time { get; private set; }

        public Player()
        {
            Name = "";
            Time = new TimeSpan(0);
        }

        public void SetName(string name)
        {
            Name = name;
        }

        public void SetTime(TimeSpan time)
        {
            Time = time;
        }
    }
}

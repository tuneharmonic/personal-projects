﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwertyCommandos.Models.Interfaces
{
    public interface IPlayer
    {
        void SetName(string name);
        void SetTime(TimeSpan time);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EntityWPFExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        QATestToolEntities dataEntities = new QATestToolEntities();
        List<ProductQuantity> orders = new List<ProductQuantity>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DbSet<Product> products = dataEntities.Products;

            // Get an abstraction of all products
            //var query = products.Select(p => new { p.ProductID, p.Name, p.Description, p.Price, p.ProductDepartment.DepartmentName, p.Brand });

            dataGrid1.ItemsSource = products.ToList();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Product prod = (Product)dataGrid1.SelectedItem;
            if (int.TryParse(quantityInput.Text, out int quan) && prod != null)
            {
                if (quan > 0)
                {
                    if (orders.Exists(pq => pq.Product.ProductID == prod.ProductID))
                    {
                        orders.Find(pq => pq.Product.ProductID == prod.ProductID).Quantity += quan;
                    }
                    else
                    {
                        orders.Add(new ProductQuantity(prod, quan));
                    }
                }
            }

            dataGrid2.ItemsSource = orders
                .Select(pq => new
                {
                    pq.Product.Name,
                    pq.Product.Price,
                    pq.Quantity
                })
                .ToList();

            totalPrice.Text = orders.Sum(pq => pq.Product.Price * pq.Quantity).ToString("c");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace KerbalHelper
{
    public static class DeltaV
    {
        /// <summary>
        /// Calculates the Delta-v of a rocket
        /// </summary>
        /// <param name="startMass">The initial mass of the craft</param>
        /// <param name="endMass">The final mass of the craft</param>
        /// <param name="isp">The average specific impulse of all engines</param>
        /// <param name="g">The acceleration of gravity at the surface of some body (default Earth)</param>
        /// <returns>The total Delta-v in m/s</returns>
        public static double Calculate(double startMass, double endMass, double isp, double g = 9.8d)
        {
            // Tsiolkovsy Rocket Equation!
            return (g * isp) * (Math.Log(startMass / endMass));
        }
    }
}
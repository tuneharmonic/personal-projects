﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace KerbalHelper
{
    [Activity(Label = "Kerbal Helper", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Grab resources
            EditText initialMassInput = FindViewById<EditText>(Resource.Id.InitialMassInput);
            EditText finalMassInput = FindViewById<EditText>(Resource.Id.FinalMassInput);
            EditText ispInput = FindViewById<EditText>(Resource.Id.IspInput);
            EditText gravityInput = FindViewById<EditText>(Resource.Id.GravityInput);
            Button calcButton = FindViewById<Button>(Resource.Id.CalculateButton);
            TextView resultView = FindViewById<TextView>(Resource.Id.DeltaVResult);

            resultView.Text = "";

            calcButton.Click += delegate
            {
                // Validate inputs
                bool initialMassValid = double.TryParse(initialMassInput.Text, out double initialMass);
                bool finalMassValid = double.TryParse(finalMassInput.Text, out double finalMass);
                bool ispValid = double.TryParse(ispInput.Text, out double isp);
                bool gravityValid = double.TryParse(gravityInput.Text, out double gravity);

                if (initialMassValid && finalMassValid && ispValid && gravityValid) // If all inputs are valid
                {
                    if (isp > 0 && finalMass > 0 && initialMass > 0 && gravity > 0) // And all numbers are > 0
                    {
                        // Calculate Delta-V
                        double result = DeltaV.Calculate(initialMass, finalMass, isp, gravity);
                        resultView.Text = $"{result.ToString()} m/s";
                    }
                }
            };
        }
    }
}


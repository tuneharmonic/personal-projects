///scr_squares()
d = point_direction(x1,y1,x,y);
l = 1;
a1 = x1 + lengthdir_x(l, d);
b1 = y1 + lengthdir_y(l, d);
while (!collision_point(a1,b1,ball,false,true)) {
l++;
a1 = x1 + lengthdir_x(l, d);
b1 = y1 + lengthdir_y(l, d);
}
l = 1;
a2 = a1 + lengthdir_x(l, d);
b2 = b1 + lengthdir_y(l, d);
while (collision_point(a2,b2,ball,false,true)) {
l++;
a2 = a1 + lengthdir_x(l, d);
b2 = b1 + lengthdir_y(l, d);
}
a3 = (a1 + a2)/2;
b3 = (b1 + b2)/2;

//Create Square coords
l = ball.hyp;
a4 = (a3 + lengthdir_x(l,(d+180)));
b4 = (b3 + lengthdir_y(l,(d+180)));

a5 = (a3 + lengthdir_x(l, d));
b5 = (b3 + lengthdir_y(l, d));

a6 = (a4 + lengthdir_x(l*2, (d+90)));
b6 = (b4 + lengthdir_y(l*2, (d+90)));

a7 = (a5 + lengthdir_x(l*2, (d+90)));
b7 = (b5 + lengthdir_y(l*2, (d+90)));

a8 = (a4 + lengthdir_x(l*2, (d+270)));
b8 = (b4 + lengthdir_y(l*2, (d+270)));

a9 = (a5 + lengthdir_x(l*2, (d+270)));
b9 = (b5 + lengthdir_y(l*2, (d+270)));

slicer.slice = true;

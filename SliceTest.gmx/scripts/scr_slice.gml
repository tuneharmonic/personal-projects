///scr_slice()
//Define slice coordinates
xoff = ball.sprite_xoffset;
yoff = ball.sprite_yoffset
targetx = ball.x - xoff;
targety = ball.y - yoff;
targetw = ball.sprite_width;
targeth = ball.sprite_height;
//Create first slice
surface_set_target(surface);
draw_clear(c_fuchsia);
with ball {
    draw_self();
}
col = draw_get_color();
draw_set_colour(c_fuchsia);
draw_triangle(player.a4, player.b4, player.a6, player.b6, player.a5, player.b5, false);
draw_triangle(player.a5, player.b5, player.a7, player.b7, player.a6, player.b6, false);
draw_set_colour(col);
spr1 = sprite_create_from_surface(surface,targetx,targety,targetw,targeth,true,false,xoff,yoff);
object_set_sprite(slice1, spr1);
//Create second slice
draw_clear(c_fuchsia);
with ball {
    draw_self();
}
col = draw_get_color();
draw_set_colour(c_fuchsia);
draw_triangle(player.a4, player.b4, player.a8, player.b8, player.a5, player.b5, false);
draw_triangle(player.a5, player.b5, player.a9, player.b9, player.a8, player.b8, false);
draw_set_colour(col);
spr2 = sprite_create_from_surface(surface,targetx,targety,targetw,targeth,true,false,xoff,yoff);
object_set_sprite(slice2, spr2);
surface_reset_target();

instance_create(ball.x, ball.y, slice1);
instance_create(ball.x, ball.y, slice2);

with ball instance_destroy();
slicer.slice = false;

﻿using ConsoleBlasters.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleBlasters.Engine
{
    public class EventManager
    {
        public InputManager Input { get; set; }
        public GridManager Grid { get; set; }
        private Random rng = new Random();

        public EventManager()
        {
            Input = new InputManager();
            Grid = new GridManager();
        }

        public void Start()
        {
            int fps = int.Parse(ConfigurationManager.AppSettings["FPS"]);
            bool keepGo = true;
            //Init stuff
            do
            {
                keepGo = Step(fps);
                //Console.WriteLine("Step");
            } while (keepGo);

            Console.WriteLine("Goodbye!");
            Console.ReadKey();
        }

        private bool Step(int fps)
        {

            //Get Player input

            List<Vector> vectors = Input.GetInput();
            Vector moveVec = vectors[0];
            Vector shootVec = vectors[1];
            //Vector shootVec = Input.GetShots();

            //Move player, enemies
            Grid.Player.Move(moveVec.X, moveVec.Y);

            //foreach (var enemy in Grid.Enemies)
            //{
            //    if (enemy.moveCount == enemy.moveTurn)
            //    {
            //        enemy.moveCount = 0;
            //        enemy.Velocity.X = rng.Next(-1, 2);
            //        enemy.Velocity.Y = rng.Next(-1, 2);
            //    }
            //    else
            //    {
            //        enemy.moveCount++;
            //    }
            //}

            //Shoot lasers
            if (shootVec.X != 0 || shootVec.Y != 0)
            {
                Vector posVec = new Vector(Grid.Player.Coordinates.X + shootVec.X, 
                                Grid.Player.Coordinates.Y + shootVec.Y);
                Grid.Lasers.Add(new Laser(shootVec, posVec));
            }

            if (Grid.Enemies.Count < 5)
            {
                if (rng.Next(0, 100) > 80)
                {
                    Grid.Enemies.Add(new BaseEnemy());
                }
            }
            //Update, draw the grid
            Grid.UpdateGrid();
            Grid.DrawGrid();

            Thread.Sleep(Convert.ToInt32(1000/fps));
            return true;
        }
    }
}

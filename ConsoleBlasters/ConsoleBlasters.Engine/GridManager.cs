﻿using ConsoleBlasters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlasters.Engine
{
    public class GridManager
    {
        public List<BaseEnemy> Enemies = new List<BaseEnemy>();
        public List<Laser> Lasers = new List<Laser>();
        public HumanPlayer Player = new HumanPlayer();
        string[,] _positions = new string[10, 10];

        public void UpdateGrid()
        {
            _positions = new string[10, 10];
            //Update Enemies
            for (int i = 0; i < Enemies.Count; i++)
            {
                //bounce off walls
                if ((Enemies[i].Coordinates.X == 0 && Enemies[i].Velocity.X < 0) || (Enemies[i].Coordinates.X == 9 && Enemies[i].Velocity.X > 0))
                {
                    Enemies[i].Velocity.X *= -1;
                }
                if ((Enemies[i].Coordinates.Y == 0 && Enemies[i].Velocity.Y < 0) || (Enemies[i].Coordinates.Y == 9 && Enemies[i].Velocity.Y > 0))
                {
                    Enemies[i].Velocity.Y *= -1;
                }
                _positions[Enemies[i].Coordinates.X, Enemies[i].Coordinates.Y] = Enemies[i].Character.ToString();

                //remove any Enemies out of bounds
                if (Enemies[i].Coordinates.X > 9 || Enemies[i].Coordinates.Y > 9 ||
                    Enemies[i].Coordinates.X < 0 || Enemies[i].Coordinates.Y < 0)
                {
                    Enemies.Remove(Enemies[i]);
                    continue;
                }

                //change position based on velocity
                Enemies[i].Coordinates.X += Enemies[i].Velocity.X;
                Enemies[i].Coordinates.Y += Enemies[i].Velocity.Y;
            }
            //Update Lasers
            for (int i = 0; i < Lasers.Count; i++)
            {
                //remove any Actors out of bounds
                if (Lasers[i].Coordinates.X > 9 || Lasers[i].Coordinates.Y > 9 || 
                    Lasers[i].Coordinates.X < 0 || Lasers[i].Coordinates.Y < 0)
                {
                    Lasers.Remove(Lasers[i]);
                    continue;
                }
                //set grid position
                _positions[Lasers[i].Coordinates.X, Lasers[i].Coordinates.Y] = Lasers[i].Character.ToString();
                //check for collisions - tbd
                for (int j = 0; j < Enemies.Count; j++)
                {
                    if (Lasers[i].Coordinates.Equals(Enemies[j].Coordinates))
                    {
                        Enemies.Remove(Enemies[j]);
                        Player.Score++;
                        continue;
                    }
                }
                //change position based on velocity
                Lasers[i].Coordinates.X += Lasers[i].Velocity.X;
                Lasers[i].Coordinates.Y += Lasers[i].Velocity.Y;
            }

            //Update Player
            _positions[Player.Coordinates.X, Player.Coordinates.Y] = Player.Character.ToString();
        }

        public void DrawGrid()
        {
            Console.Clear();
            //Steps to draw the grid and all Actors on the screen
            for (int j = 0; j < 10; j++) //For every row (Y)
            {
                for (int i = 0; i < 10; i++) //For every column (X)
                {
                    string drawMe = _positions[i, j];
                    if (string.IsNullOrEmpty(drawMe))
                    {
                        drawMe = " ";
                    }
                    Console.Write("{0,3}", drawMe);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.Write("Score: {0}", Player.Score.ToString());
        }
    }
}

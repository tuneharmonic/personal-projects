﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleBlasters.Models;

namespace ConsoleBlasters.Engine
{
    public class InputManager
    {
        public List<Vector> GetInput()
        {
            List<Vector> vectors = new List<Vector>()
            {
                new Vector(),
                new Vector()
            };
            ConsoleKeyInfo cki;

            if (Console.KeyAvailable)
            {
                cki = Console.ReadKey(true);

                vectors[0].X = Convert.ToInt16(cki.KeyChar == 'd') - Convert.ToInt16(cki.KeyChar == 'a');    //Left is negative, Right is positive
                vectors[0].Y = Convert.ToInt16(cki.KeyChar == 's') - Convert.ToInt16(cki.KeyChar == 'w');    //Up is negative, Down is positive

                vectors[1].X = Convert.ToInt16(cki.Key == ConsoleKey.RightArrow) - Convert.ToInt16(cki.Key == ConsoleKey.LeftArrow);    //Left is negative, Right is positive
                vectors[1].Y = Convert.ToInt16(cki.Key == ConsoleKey.DownArrow) - Convert.ToInt16(cki.Key == ConsoleKey.UpArrow);    //Up is negative, Down is positive
            }

            return vectors;
        }

        //public Vector GetShots()
        //{
        //    Vector vec = new Vector();
        //    ConsoleKeyInfo cki;

        //    if (Console.KeyAvailable)
        //    {
        //        cki = Console.ReadKey(true);

        //        vec.X = Convert.ToInt16(cki.Key == ConsoleKey.RightArrow) - Convert.ToInt16(cki.Key == ConsoleKey.LeftArrow);    //Left is negative, Right is positive
        //        vec.Y = Convert.ToInt16(cki.Key == ConsoleKey.DownArrow) - Convert.ToInt16(cki.Key == ConsoleKey.UpArrow);    //Up is negative, Down is positive
        //    }

        //    return vec;
        //}
    }
}

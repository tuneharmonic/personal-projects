﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlasters.CLI
{
    class GameController
    {
        public void MainMenu()
        {
            Console.WriteLine("Welcome to Console Blasters!\n");
            Console.WriteLine("Would you like to play a game of skill?");
            if (PromptYesNo())
            {
                Console.WriteLine("Sure!");
                StartGame();

            }
            else
            {
                Console.WriteLine("Boo!");
            }
            Console.WriteLine("\nGoodbye!");
            Console.ReadKey();
        }

        public bool PromptYesNo()
        {
            return Console.ReadLine().ToLower().StartsWith("y");
        }

        public void StartGame()
        {
            //Set up the Game Area
            Console.Clear();

            //Set up the player
            //Input handling?
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlasters.Models
{
    public interface IActor
    {
        Vector Coordinates { get; set; }
        Vector Velocity { get; set; }
        char Character { get; }
    }
}

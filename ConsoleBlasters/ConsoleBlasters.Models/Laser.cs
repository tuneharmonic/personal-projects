﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlasters.Models
{
    public class Laser : IActor
    {
        public Vector Coordinates { get; set; }
        public Vector Velocity { get; set; }
        public char Character { get { return '*'; } }

        public Laser(Vector velocity, Vector coords)
        {
            Velocity = new Vector();
            Velocity.X = velocity.X;
            Velocity.Y = velocity.Y;

            Coordinates = new Vector();
            Coordinates.X = coords.X;
            Coordinates.Y = coords.Y;
        }

        public Laser(int velX, int velY, int posX, int posY)
        {
            Velocity = new Vector();
            Velocity.X = velX;
            Velocity.Y = velY;

            Coordinates = new Vector();
            Coordinates.X = posX;
            Coordinates.Y = posY;
        }
    }
}

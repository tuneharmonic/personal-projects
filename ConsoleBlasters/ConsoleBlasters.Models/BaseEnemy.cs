﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlasters.Models
{
    public class BaseEnemy : IActor
    {
        private Random rng;
        public Vector Coordinates { get; set; }
        public Vector Velocity { get; set; }

        public int moveTurn = 5;

        public int moveCount = 0;

        public char Character { get { return 'X'; } }

        public BaseEnemy()
        {
            Random rng = new Random();
            Coordinates = new Vector(rng.Next(0,10), rng.Next(0,10));
            Velocity = new Vector(rng.Next(-1, 2), rng.Next(-1, 2));
        }
    }
}

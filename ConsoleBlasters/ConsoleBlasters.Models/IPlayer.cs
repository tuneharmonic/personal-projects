﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlasters.Models
{
    public interface IPlayer
    {
        void Move(int x, int y);
    }
}

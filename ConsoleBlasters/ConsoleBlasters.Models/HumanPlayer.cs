﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlasters.Models
{
    public class HumanPlayer : IPlayer, IActor
    {
        public char Character { get; private set; }
        public Vector Coordinates { get; set; }
        public Vector Velocity { get; set; }
        public int Score { get; set; }

        public void Move(int x, int y)
        {
            Coordinates.X = Math.Max(Coordinates.X + x, 0);
            Coordinates.X = Math.Min(Coordinates.X, 9);

            Coordinates.Y = Math.Max(Coordinates.Y + y, 0);
            Coordinates.Y = Math.Min(Coordinates.Y, 9);
        }

        public HumanPlayer()
        {
            Coordinates = new Vector();
            Velocity = new Vector();
            Character = 'O';
            Score = 0;
        }
    }
}

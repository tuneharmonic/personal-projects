﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	private Rigidbody2D _rb2d;
	public float Speed = 3;
	public Text CountText;
	public Text WinText;
	private int _count;

	void Start()
	{
		_rb2d = GetComponent<Rigidbody2D> ();
		_count = 0;
		WinText.text = "";
		SetCountText ();
	}

	void FixedUpdate()
	{
		float xMovement = Input.GetAxis ("Horizontal") * Speed;
		float yMovement = Input.GetAxis ("Vertical") * Speed;
		Vector2 movement = new Vector2 (xMovement, yMovement);
		_rb2d.AddForce (movement);

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("PickUp")) 
		{
			other.gameObject.SetActive (false);
			_count++;
			SetCountText ();
		}
	}

	void SetCountText()
	{
		CountText.text = "Count: " + _count.ToString ();
		if (_count >= 11) {
			WinText.text = "You Win!";
		}
	}
}
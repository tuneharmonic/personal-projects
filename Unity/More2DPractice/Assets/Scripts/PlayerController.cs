﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private Rigidbody2D rb2d;

	void Start()
	{
		rb2d = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate()
	{
        bool jumping = Input.GetKeyDown(KeyCode.W);
        bool reset = Input.GetKeyDown(KeyCode.R);
        bool quit = Input.GetKeyDown(KeyCode.Escape);


        float moveX = Input.GetAxis ("Horizontal");
        //float moveY = Input.GetAxis ("Vertical");
        rb2d.AddForce(new Vector2(moveX * 10f, 0f));

        Vector2 oldVelocity = rb2d.velocity;

        float ySpeed = jumping ? oldVelocity.y + 15f : oldVelocity.y;
        
        rb2d.velocity = new Vector2(oldVelocity.x, ySpeed);

        if (reset)
        {
            transform.position = new Vector3();
            transform.rotation = new Quaternion();
            rb2d.angularVelocity = 0f;
            rb2d.velocity = new Vector2();
        }

        if (quit)
        {
            Application.Quit();
        }
	}
}

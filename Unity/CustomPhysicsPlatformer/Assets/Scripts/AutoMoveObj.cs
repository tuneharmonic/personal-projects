﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMoveObj : CustomPhyObj {

    public float moveSpeed = -7f;

    // Update is called once per frame
    protected override void ComputeVelocity()
    {
        targetXVelocity = moveSpeed;
    }
}

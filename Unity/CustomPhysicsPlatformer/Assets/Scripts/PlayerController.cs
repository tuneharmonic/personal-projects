﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CustomPhyObj
{
    public int jumpCount = 2;
    public float jumpSpeed = 8f;
    public float maxSpeed = 8f;

    private int jumps;

    protected override void Start()
    {
        base.Start();
        jumps = jumpCount;
    }

    protected override void ComputeVelocity()
    {
        targetXVelocity = Input.GetAxis("Horizontal") * maxSpeed;
        if (grounded) jumps = jumpCount;
        if (Input.GetButtonDown("Jump") && jumps > 0)
        {
            AddJumpVelocity(jumpSpeed);
            jumps--;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y *= .5f;
            }
        }
    }

    public void AddJumpVelocity(float jumpVel)
    {
        velocity.y += jumpVel;
    }
}
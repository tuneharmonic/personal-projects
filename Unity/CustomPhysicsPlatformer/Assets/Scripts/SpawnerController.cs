﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

    public GameObject[] enemies;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.R) && GameObject.FindWithTag("Enemy") == null)
        {
            // pick a random enemy
            int enemyIndex = Mathf.RoundToInt(Random.Range(0, enemies.Length));
            if (enemies[enemyIndex].CompareTag("Enemy"))
            {
                // respawn the enemy
                Instantiate(enemies[enemyIndex], transform.position, transform.rotation, transform);
            }
        }
	}
}

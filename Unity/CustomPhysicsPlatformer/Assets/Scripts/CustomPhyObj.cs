﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomPhyObj : MonoBehaviour {

    public float minGroundNormalY = 0.65f;
    public float gravityModifier = 1.0f;

    protected float targetXVelocity;
    protected Vector2 velocity;
    protected Rigidbody2D rb2d;
    protected ContactFilter2D contactFilter;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);
    protected bool grounded;
    protected Vector2 groundNormal;

    protected const float minMoveDistance = 0.001f;
    protected const float shellRadius = 0.01f;

	// Use this for initialization
	protected virtual void Start () {
        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
	}

    void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
        groundNormal = Vector2.up;
    }

    void Update()
    {
        targetXVelocity = 0f;
        ComputeVelocity();
    }

    protected virtual void ComputeVelocity()
    {

    }

    // Update is called once per frame
    void FixedUpdate () {
        // Calculate velocity and change in position
        velocity.x = targetXVelocity;
        velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
        Vector2 deltaPosition = velocity * Time.deltaTime;

        // X-Axis movement
        Vector2 groundAngle = new Vector2(groundNormal.y, -groundNormal.x);
        Vector2 xMove = groundAngle * deltaPosition.x;
        Movement(xMove, false);

        // Y-Axis movement
        grounded = false;
        Vector2 yMove = Vector2.up * deltaPosition.y;
        Movement(yMove, true);
	}

    void Movement(Vector2 move, bool yMovement)
    {
        float distance = move.magnitude;
        // if distance to move is too small, don't check collisions
        if (distance > minMoveDistance)
        {
            // check and store collisions
            int count = rb2d.Cast(move, contactFilter, hitBuffer, distance + shellRadius);
            hitBufferList.Clear();
            for (int i = 0; i < count; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }

            // for each collision...
            for (int i = 0; i < hitBufferList.Count; i++)
            {
                Vector2 currentNormal = hitBufferList[i].normal;
                // if the angle is shallow enough to stand on...
                if (currentNormal.y > minGroundNormalY)
                {
                    grounded = true;
                    if (yMovement)
                    {
                        // store current ground normal, correct x component to act as flat ground
                        groundNormal = currentNormal;
                        currentNormal.x = 0f;
                    }
                }
                // if velocity suggests a collision...
                float projection = Vector2.Dot(velocity, currentNormal);
                if (projection < 0)
                {
                    // subtract difference in velocity and collision from velocity
                    velocity -= projection * currentNormal;
                }
                // modify distance to move based on collision
                float modifiedDistance = hitBufferList[i].distance - shellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }
        }

        rb2d.position += move.normalized * distance;
    }
}

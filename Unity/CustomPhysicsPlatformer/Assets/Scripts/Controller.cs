﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

    private Rigidbody2D rb2d;

    public float jumpSpeed = 8f;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
	}

    private void FixedUpdate()
    {
        float xSpeed = Input.GetAxis("Horizontal") * 5f;

        float oldYSpeed = rb2d.velocity.y;
        float ySpeed = Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) ? oldYSpeed + jumpSpeed : oldYSpeed;

        rb2d.velocity = new Vector2(xSpeed, ySpeed);
    }
}

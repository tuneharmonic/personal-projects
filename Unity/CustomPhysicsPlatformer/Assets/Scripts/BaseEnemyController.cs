﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemyController : CustomPhyObj {

    public float moveSpeed = 6f;
    public float moveRight = 1f;
    public float collisionShell = 0.1f;

    protected bool killed = false;

    // Update is called once per frame
    protected override void ComputeVelocity()
    {
        if (killed)
        {
            gameObject.SetActive(false);
        }
        if (velocity.x == 0)
        {
            moveRight = -moveRight;
        }
        targetXVelocity = moveRight * moveSpeed;

        int playerCount = rb2d.Cast(Vector2.up, contactFilter, hitBuffer, collisionShell);
        if (playerCount > 0)
        {
            killed = true;
            // player rebound
            var playerController = hitBuffer[0].rigidbody.gameObject.GetComponent<PlayerController>();
            playerController.AddJumpVelocity(playerController.jumpSpeed / 2f);
        }
    }
}

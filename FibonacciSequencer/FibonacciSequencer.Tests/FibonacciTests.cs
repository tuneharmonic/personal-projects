﻿using FibonacciSequencer.BLL;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequencer.Tests
{
    [TestFixture]
    public class FibonacciTests
    {
        [Test]
        public void RecursiveNumberAtReturnsCorrectValue()
        {
            Fibonacci fib = new Fibonacci();
            Assert.AreEqual(0, fib.RecursiveNumberAt(-5));
            Assert.AreEqual(0, fib.RecursiveNumberAt(0));
            Assert.AreEqual(1, fib.RecursiveNumberAt(1));
            Assert.AreEqual(3, fib.RecursiveNumberAt(4));
            Assert.AreEqual(34, fib.RecursiveNumberAt(9));
        }

        [Test]
        public void IterativeNumberAtReturnsCorrectValue()
        {
            Fibonacci fib = new Fibonacci();
            Assert.AreEqual(0, fib.IterativeNumberAt(-5));
            Assert.AreEqual(0, fib.IterativeNumberAt(0));
            Assert.AreEqual(1, fib.IterativeNumberAt(1));
            Assert.AreEqual(3, fib.IterativeNumberAt(4));
            Assert.AreEqual(34, fib.IterativeNumberAt(9));
        }

        [TestCase(5, new int[] { 0, 1, 1, 2, 3, 5 })]
        [TestCase(7, new int[] { 0, 1, 1, 2, 3, 5, 8, 13 })]
        [TestCase(9, new int[] { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 })]
        public void NumbersThroughReturnsCorrectValues(int n, int[] provided)
        {
            List<int> expected = provided.ToList();

            Fibonacci fib = new Fibonacci();

            Assert.AreEqual(expected, fib.NumbersThrough(n));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequencer.BLL
{
    public class Fibonacci
    {
        public int RecursiveNumberAt(int n)
        {
            if (n <= 0) return 0;
            else if (n == 1) return 1;
            else return RecursiveNumberAt(n - 2) + RecursiveNumberAt(n - 1);
        }

        public int IterativeNumberAt(int n)
        {
            int result = 0;
            int currentNumber = 0;
            int nextNumber = 1;

            for (int i = 0; i < n; i++)
            {
                result = nextNumber;
                nextNumber += currentNumber;
                currentNumber = result;
            }

            return result;
        }

        public List<int> NumbersThrough(int n)
        {
            List<int> fibNumbers = new List<int>();

            for (int i = 0; i <= n; i++)
            {
                fibNumbers.Add(RecursiveNumberAt(i));
            }

            return fibNumbers;
        }
    }
}

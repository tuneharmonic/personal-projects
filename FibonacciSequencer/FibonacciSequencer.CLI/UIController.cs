﻿using FibonacciSequencer.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequencer.CLI
{
    class UIController
    {
        Fibonacci _fib;
        private const string SEP_BAR = "===============================================";

        public UIController()
        {
            _fib = new Fibonacci();
        }

        public void MainMenu()
        {
            string prompt;
            do
            {
                Console.Clear();
                Console.WriteLine("Welcome to Fibonacci Sequencer, v1.0!");
                Console.WriteLine(SEP_BAR);
                Console.WriteLine();
                Console.WriteLine("Please choose what you would like to do:");
                Console.WriteLine();
                Console.WriteLine("1) Find a number in the sequence recursively");
                Console.WriteLine("2) Find a number in the sequence iteratively");
                Console.WriteLine("3) Print all numbers to a point in the sequence");
                Console.WriteLine();
                Console.WriteLine("Q) Quit Program");
                Console.WriteLine();
                Console.WriteLine(SEP_BAR);
                Console.WriteLine();
                Console.Write("Enter Choice: ");
                prompt = Console.ReadLine().Trim().ToUpper();

                Console.Clear();
                switch (prompt)
                {
                    case "1":
                        RecursiveNumberAt();
                        break;
                    case "2":
                        IterativeNumberAt();
                        break;
                    case "3":
                        NumbersThrough();
                        break;
                    case "Q":
                        Console.WriteLine("Thank you for using Fibonacci Sequencer!");
                        Console.WriteLine("Press any key to exit...");
                        Console.ReadKey();
                        break;
                    default:
                        Console.WriteLine("That is not a valid choice, please try again.");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        break;
                }

            } while (prompt != "Q");
        }

        public int PromptForN()
        {
            int n;
            while (!int.TryParse(Console.ReadLine(), out n))
            {
                Console.WriteLine("Give me a whole number! Zero or greater, preferrably.");
            }
            Console.WriteLine();
            return n;
        }

        public void NumbersThrough()
        {
            Console.WriteLine("Please give me a position in the Fibonacci Sequence to return the values through:");
            int n = PromptForN();

            List<int> fibNumbers = _fib.NumbersThrough(n);

            for (int i = 0; i < fibNumbers.Count; i++)
            {
                Console.WriteLine("Number {0} = {1}", i, fibNumbers[i]);
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        public void IterativeNumberAt()
        {
            Console.WriteLine("Please give me a position in the Fibonacci Sequence to return the value of, iteratively:");
            int n = PromptForN();
            Console.WriteLine($"The number at position {n} in the sequence is {_fib.IterativeNumberAt(n)}.");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        public void RecursiveNumberAt()
        {
            Console.WriteLine("Please give me a position in the Fibonacci Sequence to return the value of, recursively:");
            int n = PromptForN();
            Console.WriteLine($"The number at position {n} in the sequence is {_fib.RecursiveNumberAt(n)}.");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}

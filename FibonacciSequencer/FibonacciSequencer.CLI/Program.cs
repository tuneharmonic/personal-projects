﻿using FibonacciSequencer.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequencer.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            UIController ui = new UIController();
            ui.MainMenu();
        }
    }
}

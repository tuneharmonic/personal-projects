﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGEngine;
using RPGEngine.Data;

namespace RPGEngine.GLL
{
    public class CampaignController
    {
        private CampaignDatastore myData = new CampaignDatastore();

        public Campaign LoadCampaign(string name)
        {
            // TODO? - Add logic for returning null
            return myData.LoadCampaign(name);
        }

        public void AddCampaign(Campaign camp)
        {
            // TODO - Add logic to check for already existing campaign
            myData.AddNewCampaign(camp);
        }

        public void AddCampaign(string name, Scene scene, Party party, QuestLog log)
        {
            Campaign camp = new Campaign();
            camp.Name = name;
            camp.Battlefield = scene;
            camp.PlayerParty = party;
            camp.Questlog = log;
            // TODO - Add logic to check for already existing campaign
            myData.AddNewCampaign(camp);
        }

        public void EditCampaign(string name, Campaign editedCamp)
        {
            // TODO - Add edit campaign logic
        }

        public void DeleteCampaign(string name)
        {
            // TODO - Add delete campaign logic
        }
    }
}

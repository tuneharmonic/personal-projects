﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            bool keepGoing = true;
            while (keepGoing)
            {
                if (int.TryParse(Console.ReadLine(), out int result))
                {
                    if (result == 0)
                    {
                        Console.WriteLine("Goodbye!");
                        keepGoing = false;
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("Unrecognized Command");
                    }
                }
                else
                {
                    Console.WriteLine("Enter whole numbers only");
                }
            }
        }
    }
}

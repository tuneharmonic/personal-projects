﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGEngine;
using RPGEngine.GLL;

namespace RPGEngine.CLI
{
    class GameController
    {
        private CampaignController campaign = new CampaignController();

        public void MainMenu()
        {
            // TODO - display a menu with a list of GM options

            // start a new campaign...
            // load a saved campaign...
            // delete a campaign?
            // quit application
        }

        public void StartNewCampaign()
        {
            // TODO - Start a new campaign
        }

        public void LoadCampaign()
        {
            // TODO - Load saved campaign

            // returns a campiagn?
        }

        public void CampaignMainMenu()
        {
            // from MainMenu()
            // TODO - display a menu of campaign-specific options

            // change campaign name
            // edit party
            // edit scene
            // edit quest log
            // save campaign
            // exit to main menu (saves campaign)
        }

        public void ChangeCampaignName(string newName)
        {
            // from CampaignMainMenu()
            // TODO - change campaign name
        }

        public void EditPartyMenu()
        {
            // from CampaignMainMenu()
            // TODO - display a menu of party editing options

            // add new player
            // edit a player
            // remove a player
            // view a player?
        }


    }
}

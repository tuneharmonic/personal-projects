﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    //public class Cover
    //{
    //    public Vector Size { get; set; } = new Vector()
    //    public Vector Coordinates { get; set; }
    //    public float CoverMod { get; set; }
    //    public bool Destructable { get; set; }
    //    public int Health { get; set; }
    //}

    public class Scene
    {
        public string SceneName { get; set; }
        public Vector GridSize { get; set; }
        public List<Creature> NPCs { get; set; } 

        public Scene()
        {
            GridSize = new Vector();
            NPCs = new List<Creature>();
        }
    }

    public class Quest : Item
    {
        public string Objective { get; set; }
        public string Status { get; set; }
    }

    public class QuestLog
    {
        public List<Quest> Quests { get; set; }

        public QuestLog()
        {
            Quests = new List<Quest>();
        }
    }

    public class Campaign
    {
        public string Name { get; set; }
        public Party PlayerParty { get; set; } = new Party();
        public Scene Battlefield { get; set; } = new Scene();
        public QuestLog Questlog { get; set; } = new QuestLog();

        public Campaign()
        {
            PlayerParty = new Party();
            Battlefield = new Scene();
            Questlog = new QuestLog();
        }
    }

    public class Party
    {
        public List<PlayerCharacter> Players { get; set; }
        public string PartyName { get; set; }

        public PlayerCharacter GetPlayer(string name)
        {
            var player = Players.FirstOrDefault(p => p.Name.ToLower() == name.ToLower());
            if (player != null)
            {
                return player;
            }
            throw new ApplicationException("Player: '" + name + "' not found in Player List");
        }

        public Party()
        {
            Players = new List<PlayerCharacter>();
        }
    }

    public class PlayerCharacter : Character
    {
        public string PlayerName { get; set; }
    }

    public enum AttributeType
    {
        Strength,
        Dexterity,
        Constitution,
        Wisdom,
        Intelligence,
        Charisma
    }

    public class AttributeStore
    {
        private List<int> attributes;

        public int GetAttribute(AttributeType type)
        {
            return attributes[Convert.ToInt32(type)];
        }

        public int GetAttribute(int type)
        {
            return attributes[type];
        }

        public List<int> GetAllAttributes()
        {
            return attributes;
        }

        public void AddToAttribute(AttributeType type, int pointsToAdd)
        {
            attributes[Convert.ToInt32(type)] += pointsToAdd;
        }

        public void AddToAttribute(int type, int pointsToAdd)
        {
            attributes[type] += pointsToAdd;
        }

        public void SetAttribute(AttributeType type, int newPoints)
        {
            attributes[Convert.ToInt32(type)] = newPoints;
        }

        public void SetAttribute(int type, int newPoints)
        {
            attributes[type] = newPoints;
        }

        public AttributeStore()
        {
            attributes = new List<int>()
            {
                10, 10, 10, 10, 10, 10
            };
        }
        public AttributeStore(int num)
        {
            attributes = new List<int>()
            {
                num, num, num, num, num, num
            };
        }
        public AttributeStore(int str, int dex, int con, int wis, int intel, int cha)
        {
            attributes = new List<int>()
            {
                str, dex, con, wis, intel, cha
            };
        }
    }

    public class Character : Creature
    {
        public string Class { get; set; }
        public int Shield { get; set; }
        public float ShieldRecharge { get; set; }
        public float ShieldTimer { get; set; }
        public int TotalShield { get; set; }
        public float CoverMod { get; set; }
        public Vector CoverDir { get; set; }
        public Weapon CurrentWeapon { get; set; }
        public List<Ability> Abilities { get; set; }

        protected bool inCover = false;

        public void GetToCover(float coverMod, Vector coverDir)
        {
            inCover = true;
            CoverMod = coverMod;
            CoverDir = coverDir;
        }

        public void LeaveCover()
        {
            inCover = false;
            CoverMod = 0f;
            CoverDir = new Vector();
        }

        public Character() : base()
        {
            CoverDir = new Vector();
            Abilities = new List<Ability>();
        }
    }

    public class Creature
    {
        public string Name { get; set; }
        public string Race { get; set; }
        public int Age { get; set; }
        public int Level { get; set; }
        public int Health { get; set; }
        public int TotalHealth { get; set; }
        public int HitDie { get; set; }
        public int AC { get; set; }
        public int MoveSpeed { get; set; }
        public AttributeStore Attributes { get; set; }
        public List<InventoryItem> Inventory { get; set; }
        public Vector Coordinates { get; set; }

        public int GetAC()
        {
            return Attributes.GetAttribute(AttributeType.Dexterity) + 10;
        }

        public Creature()
        {
            Attributes = new AttributeStore();
            Inventory = new List<InventoryItem>();
            Coordinates = new Vector();

            //Name = "Husk";
            //Race = "Reaper";
            //Age = 0;
            //Level = 1;
            //Health = 100;
            //TotalHealth = 100;
            //HitDie = 20;
            //MoveSpeed = 6;
            //Attributes = new AttributeStore();
            //AC = GetAC();
            //Inventory = new List<InventoryItem>();
            //Inventory.Add(new InventoryItem());
            //Coordinates = new Vector();
        }
    }

    public class Armor : InventoryItem, IDefense
    {
        public float DamageReduction { get; set; }
        public float DamageAbsorbtion { get; set; }
    }

    public class Weapon : InventoryItem, IOffense
    {
        public int Damage { get; set; }
        public float Accuracy { get; set; }
    }

    public class Blade : Weapon
    {
        public float Reach { get; set; }
    }

    public class Gun : Weapon
    {
        public float FiringRate { get; set; }
        public float Range { get; set; }
        public bool UsesAmmo { get; set; }
        public int ClipSize { get; set; }
        public int CurrentClipSize { get; set; }
        public float HeatCapacity { get; set; }
        public float HeatPerShot { get; set; }
        public float CurrentHeat { get; set; }

        public Gun()
        {
            Name = "M-3 Predator";
            Description = "Standard heavy pistol of the Galactic Alliance";
            Stackable = false;
            StackSize = 1;
            Quantity = 1;
            Damage = 50;
            Accuracy = 50;
            FiringRate = 2f;
            Range = 100f;
            UsesAmmo = true;
            ClipSize = 12;
            CurrentClipSize = ClipSize;
        }
    }

    public class OffensiveAbility : Ability, IOffense
    {
        public int Damage { get; set; }
        public float Accuracy { get; set; }
    }

    public class DefensiveAbility : Ability, IDefense
    {
        public float DamageReduction { get; set; }
        public float DamageAbsorbtion { get; set; }
    }

    public class Ability : Item
    {
        public AbilityType Type { get; set; }
        public float RechargeTime { get; set; }
        public float Duration { get; set; }
        public int Level { get; set; }
    }

    public enum AbilityType
    {
        Combat,
        Tech,
        Biotic
    }

    public class Skill : Item
    {
        public int Rank { get; set; }
        public AttributeType Attribute { get; set; }
    }

    public interface IOffense
    {
        int Damage { get; set; }
        float Accuracy { get; set; }
    }

    public interface IDefense
    {
        float DamageReduction { get; set; }
        float DamageAbsorbtion { get; set; }
    }

    public class InventoryItem : Item
    {
        public bool Stackable { get; set; }
        public int Quantity { get; set; }
        public int StackSize { get; set; }
    }

    public class Item
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class Vector
    {
        public double X { get; set; }
        public double Y { get; set; }

        /// <summary>
        /// Obtain the Dot Product of this vector and another
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public double DotProduct(Vector other)
        {
            double result = X * other.X + Y * other.Y;
            return result;
        }

        public Vector Normalized()
        {
            double length = GetLength();
            return new Vector((X / length), (Y / length));
        }

        public double GetLength()
        {
            double xD = X;
            double yD = Y;
            return Math.Sqrt((X * X) + (Y * Y));
        }

        public Vector()
        {
            X = 0d;
            Y = 0d;
        }
        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGEngine;
using NUnit.Framework;

namespace RPGEngine.Tests
{
    [TestFixture]
    public class ModelTests
    {
        [TestCase]
        public void DefaultCreatureIsHusk()
        {
            var result = new Creature();
            Assert.AreEqual("Husk", result.Name);
        }

        [TestCase]
        public void DefaultCharacterIsJoe()
        {
            var result = new Character();
            Assert.AreEqual("Joe", result.Name);
        }

        [TestCase]
        public void DefaultPCIsJoeMaki()
        {
            var result = new PlayerCharacter();
            Assert.AreEqual("Joe Maki", result.Name);
        }
    }
}

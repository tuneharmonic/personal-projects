﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGEngine;

namespace RPGEngine.Data
{
    public class SceneDatastore
    {
        static List<Scene> scenes = new List<Scene>();

        public void AddNewScene(Scene scene)
        {
            if (scene != null)
            {
                scenes.Add(scene);
            }
        }

        public Scene LoadScene(string name)
        {
            return scenes.FirstOrDefault(s => s.SceneName == name);
        }

        public bool DeleteScene(string name)
        {
            var scene = LoadScene(name);
            if (scene != null)
            {
                scenes.Remove(scene);
                return true;
            }
            return false;
        }

        public bool SaveScene(Scene scene)
        {
            var sceneIndex = scenes.FindIndex(s => s.SceneName == scene.SceneName);
            if (sceneIndex >= 0)
            {
                scenes[sceneIndex] = scene;
                return true;
            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGEngine;

namespace RPGEngine.Data
{
    public class CampaignDatastore
    {
        static List<Campaign> campaignData = new List<Campaign>();

        public void AddNewCampaign(Campaign campaign)
        {
            if (campaign != null)
            {
                campaignData.Add(campaign);
            }
        }

        public Campaign LoadCampaign(string name)
        {
            return campaignData.FirstOrDefault(c => c.Name == name);
        }

        public bool DeleteCampaign(string name)
        {
            var camp = LoadCampaign(name);
            if (camp != null)
            {
                campaignData.Remove(camp);
                return true;
            }
            return false;
        }

        public bool SaveCampaign(Campaign camp)
        {
            var myCampIndex = campaignData.FindIndex(c => c.Name == camp.Name);
            if (myCampIndex >= 0)
            {
                campaignData[myCampIndex] = camp;
                return true;
            }
            return false;
        }
    }
}

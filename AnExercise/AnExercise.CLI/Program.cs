﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnExercise.BLL;

namespace AnExercise.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            bool repeat = true;
            do
            {
                Console.Write("Enter a number to translate to roman numeral: ");
                string product = NumeralTranslator.TranslateToNumeral(int.Parse(Console.ReadLine())).NumString;
                Console.WriteLine("The product is " + product);
                Console.ReadLine();


                Console.Write("Translate another number? (y/n): ");
                repeat = Console.ReadLine().ToLower().StartsWith("y");
            } while (repeat);
            Console.WriteLine("Goodbye!");
            Console.Read();
        }
    }
}

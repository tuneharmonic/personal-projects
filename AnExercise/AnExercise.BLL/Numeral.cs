﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnExercise.BLL
{
    public class Numeral
    {
        public int Value { get; private set; }
        public string NumString { get; private set; }

        public Numeral(int val, string numStr)
        {
            Value = val;
            NumString = numStr;
        }

        public Numeral()
        {
            Value = 0;
            NumString = "";
        }
    }
}

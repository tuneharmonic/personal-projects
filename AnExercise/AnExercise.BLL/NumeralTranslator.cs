﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnExercise.BLL
{
    public static class NumeralTranslator
    {
        public static Numeral TranslateToNumber(string input)
        {
            int total = 0;
            int highestValue = 0;
            int currentValue = 0;
            for (int i = input.Length - 1; i >= 0; i--)
            {
                if (!char.IsLetter(input[i]))
                {
                    throw new ArgumentException("All characters must be letters. Please check input.");
                }

                switch (input[i].ToString().ToLower())
                {
                    case "i":
                        currentValue = 1;
                        break;
                    case "v":
                        currentValue = 5;
                        break;
                    case "x":
                        currentValue = 10;
                        break;
                    case "l":
                        currentValue = 50;
                        break;
                    case "c":
                        currentValue = 100;
                        break;
                    case "d":
                        currentValue = 500;
                        break;
                    case "m":
                        currentValue = 1000;
                        break;
                    default:
                        throw new ArgumentException("The letter " + input[i] + " is not an accepted numeral.");
                }

                if (currentValue >= highestValue)
                {
                    total += currentValue;
                    highestValue = currentValue;
                }
                else
                {
                    total -= currentValue;
                }
            }

            return new Numeral(total, input);
        }

        public static Numeral TranslateToNumeral(int input)
        {
            int originalInput = input;
            string output = "";
            double charCount;

            if (input >= 1000)
            {
                charCount = input / 1000;
                int numOfThousands = (int)Math.Floor(charCount);
                for (int i = 0; i < numOfThousands; i++)
                {
                    output += "M";
                }
                input %= 1000;
            }
            if (input >= 900)
            {
                output += "CM";
                input -= 900;
            }
            if (input >= 500)
            {
                charCount = input / 500;
                int numOfFiveHundreds = (int)Math.Floor(charCount);
                for (int i = 0; i < numOfFiveHundreds; i++)
                {
                    output += "D";
                }
                input %= 500;
            }
            if (input >= 100)
            {
                charCount = input / 100;
                int numOfHundreds = (int)Math.Floor(charCount);
                if (numOfHundreds == 4)
                {
                    output += "CD";
                }
                else
                {
                    for (int i = 0; i < numOfHundreds; i++)
                    {
                        output += "C";
                    }
                }
                input %= 100;
            }
            if (input >= 90)
            {
                output += "XC";
                input -= 90;
            }
            if (input >= 50)
            {
                charCount = input / 50;
                int numOfFifties = (int)Math.Floor(charCount);
                for (int i = 0; i < numOfFifties; i++)
                {
                    output += "L";
                }
                input %= 50;
            }
            if (input >= 10)
            {
                charCount = input / 10;
                int numOfTens = (int)Math.Floor(charCount);
                if (numOfTens == 4)
                {
                    output += "XL";
                }
                else
                {
                    for (int i = 0; i < numOfTens; i++)
                    {
                        output += "X";
                    }
                }
                input %= 10;
            }
            if (input == 9)
            {
                output += "IX";
                input = 0;
            }
            if (input >= 5)
            {
                charCount = input / 5;
                int numOfFives = (int)Math.Floor(charCount);
                for (int i = 0; i < numOfFives; i++)
                {
                    output += "V";
                }
                input %= 5;
            }
            if (input >= 1)
            {
                charCount = input;
                int numOfOnes = (int)charCount;
                if (numOfOnes == 4)
                {
                    output += "IV";
                }
                else
                {
                    for (int i = 0; i < numOfOnes; i++)
                    {
                        output += "I";
                    }
                }
            }

            return new Numeral(originalInput, output);
        }
    }
}

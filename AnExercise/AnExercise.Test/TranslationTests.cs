﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using AnExercise.BLL;

namespace AnExercise.Test
{
    [TestFixture]
    public class TranslationTests
    {
        [TestCase("vii", 7)]
        [TestCase("XIX", 19)]
        [TestCase("vvvx", -5)]
        [TestCase("xcigm", 0)]
        [TestCase("ii4x", 0)]
        [TestCase("MDCDLXLIX", 1999)]
        public void CanTranslateToNumber(string input, int expectedOutput)
        {
            int actualOutput = NumeralTranslator.TranslateToNumber(input).Value;
            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [TestCase(1, "I")]
        [TestCase(9, "IX")]
        [TestCase(19, "XIX")]
        [TestCase(555, "DLV")]
        [TestCase(1999, "MDCDLXLIX")]
        public void CanTranslateToNumeral(int input, string expectedOutput)
        {

        }

        [Test]
        public void TestTest()
        {
            int myInt = 1;
            Assert.AreEqual(1, myInt);
        }
    }
}

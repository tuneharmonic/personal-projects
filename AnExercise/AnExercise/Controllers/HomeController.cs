﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AnExercise.BLL;

namespace AnExercise.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TranslateToNumeral()
        {
            Numeral model = new Numeral();
            return View(model);
        }

        [HttpPost]
        public ActionResult TranslateToNumeral(int num)
        {
            Numeral result = NumeralTranslator.TranslateToNumeral(num);
            return View(result);
        }

        [HttpGet]
        public ActionResult TranslateToNumber()
        {
            Numeral model = new Numeral();
            return View(model);
        }

        [HttpPost]
        public ActionResult TranslateToNumber(string str)
        {
            Numeral result = NumeralTranslator.TranslateToNumber(str);
            return View(result);
        }
    }
}